import torch
import torch.nn as nn
import math
from torch.autograd import Variable


# класс эмбединга ( сопоставление слова числовому вектору)
class Embedder(nn.Module):
    def __init__(self, vocab_size, d_model):
        """
        Инициализация
        :param vocab_size: размер словаря
        :param d_model: количество прогнозируемых признаков
        """
        super().__init__()
        self.d_model = d_model
        self.embed = nn.Embedding(vocab_size, d_model)

    def forward(self, x):
        """
        Реализация метода "вперед"
        :param x: x
        :return: x
        """
        return self.embed(x)


# Класс кодера( позиционирование)
class PositionalEncoder(nn.Module):
    def __init__(self, d_model, max_seq_len=200, dropout=0.1):
        """
        Инициализация
        :param d_model:  количество прогнозируемых признаков
        :param max_seq_len: максимальная длина последовательности
        :param dropout: коэффициент исключения
        """
        super().__init__()
        self.d_model = d_model
        self.dropout = nn.Dropout(dropout)
        pe = torch.zeros(max_seq_len, d_model)
        for pos in range(max_seq_len):
            for i in range(0, d_model, 2):
                pe[pos, i] = \
                    math.sin(pos / (10000 ** ((2 * i) / d_model)))
                pe[pos, i + 1] = \
                    math.cos(pos / (10000 ** ((2 * (i + 1)) / d_model)))
        pe = pe.unsqueeze(0)
        self.register_buffer('pe', pe)

    def forward(self, x):
        """
        Реализация метода "Вперед"
        :param x:
        :return:
        """
        # увеличить размер эмбединга
        x = x * math.sqrt(self.d_model)
        # прибавляем константу
        seq_len = x.size(1)
        pe = Variable(self.pe[:, :seq_len], requires_grad=False)
        if x.is_cuda:
            pe.cuda()
        x = x + pe
        return self.dropout(x)
