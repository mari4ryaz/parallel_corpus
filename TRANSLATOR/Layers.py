import torch
import torch.nn as nn
from Sublayers import FeedForward, MultiHeadAttention, Norm


# класс - реализация слоя для кодера
class EncoderLayer(nn.Module):
    def __init__(self, d_model, heads, dropout=0.1):
        """
        Инициализация
        """
        super().__init__()
        self.norm_1 = Norm(d_model)
        self.norm_2 = Norm(d_model)
        self.attn = MultiHeadAttention(heads, d_model, dropout=dropout)
        self.ff = FeedForward(d_model, dropout=dropout)
        self.dropout_1 = nn.Dropout(dropout)
        self.dropout_2 = nn.Dropout(dropout)

    def forward(self, x, mask):
        """
        Реализация метода "вперед"
        :param x: x
        :param mask: маска
        :return: x
        """
        x2 = self.norm_1(x)
        x = x + self.dropout_1(self.attn(x2, x2, x2, mask))
        x2 = self.norm_2(x)
        x = x + self.dropout_2(self.ff(x2))
        return x


# Класс декодера с двумя слоями внимания и одним слоем "вперед"
class DecoderLayer(nn.Module):
    def __init__(self, d_model, heads, dropout=0.1):
        """
        Инициализация
        """
        super().__init__()
        self.norm_1 = Norm(d_model)
        self.norm_2 = Norm(d_model)
        self.norm_3 = Norm(d_model)

        self.dropout_1 = nn.Dropout(dropout)
        self.dropout_2 = nn.Dropout(dropout)
        self.dropout_3 = nn.Dropout(dropout)

        self.attn_1 = MultiHeadAttention(heads, d_model, dropout=dropout)
        self.attn_2 = MultiHeadAttention(heads, d_model, dropout=dropout)
        self.ff = FeedForward(d_model, dropout=dropout)

    def forward(self, x, e_outputs, src_mask, trg_mask):
        """
        Реализация метода "Вперед"
        :param x: x
        :param e_outputs: данные
        :param src_mask: исходная маска
        :param trg_mask: целевая маска
        :return: x
        """
        x2 = self.norm_1(x)
        x = x + self.dropout_1(self.attn_1(x2, x2, x2, trg_mask))
        x2 = self.norm_2(x)
        x = x + self.dropout_2(self.attn_2(x2, e_outputs, e_outputs, src_mask))
        x2 = self.norm_3(x)
        x = x + self.dropout_3(self.ff(x2))
        return x
