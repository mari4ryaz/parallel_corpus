import pandas as pd
from torchtext import data
from Tokenize import tokenize
from Batch import MyIterator, batch_size_fn
import dill as pickle
import pathlib

def read_data(opt):
    """
    Чтение файлов с паралельным корпусом
    :param opt: парамтеры
    """
    if opt.src_data is not None:
        try:
            opt.src_data = open(opt.src_data, encoding="utf-8").read().strip().split('\n')
        except Exception as e:
            print(e)
            print("error: '" + opt.src_data + "' файл не найден")
            quit()

    if opt.trg_data is not None:
        try:
            opt.trg_data = open(opt.trg_data, encoding="utf-8").read().strip().split('\n')
        except:
            print("error: '" + opt.trg_data + "' файл не найден")
            quit()
    pass


def create_fields(opt):
    """
    Создание набора данных
    :param opt: параметры
    :return:
    """
    print("Загружаем spacy tokenizers...")

    t_src = tokenize("ru_core_news_md")
    t_trg = tokenize("en_core_web_md")

    TRG = data.Field(lower=True, tokenize=t_trg.tokenizer, init_token='<sos>', eos_token='<eos>')
    SRC = data.Field(lower=True, tokenize=t_src.tokenizer)

    if opt.load_weights is not None:
        try:
            temp = pathlib.PosixPath
            pathlib.PosixPath = pathlib.WindowsPath
            print("Загрузка сохраненного словаря...")
            SRC = pickle.load(open(f'{opt.load_weights}SRC.pkl', 'rb'))
            TRG = pickle.load(open(f'{opt.load_weights}TRG.pkl', 'rb'))
        except Exception as e:
            print(e)
            print("error opening SRC.pkl and TRG.pkl field files, please ensure they are in " + opt.load_weights)
            quit()

    return SRC, TRG


def create_dataset(opt, SRC, TRG):
    """
     Создание набора данных для обучения
    :param opt: параметры
    :param SRC: исходный набор
    :param TRG: целевой набор
    :return: количество итераций
    """
    print("подготовка набора данных... ")

    raw_data = {'src': [line for line in opt.src_data], 'trg': [line for line in opt.trg_data]}
    df = pd.DataFrame(raw_data, columns=["src", "trg"])

    mask = (df['src'].str.count(' ') < opt.max_strlen) & (df['trg'].str.count(' ') < opt.max_strlen)
    df = df.loc[mask]

    df.to_csv("translate_transformer_temp.csv", index=False)

    data_fields = [('src', SRC), ('trg', TRG)]
    train = data.TabularDataset('./translate_transformer_temp.csv', format='csv', fields=data_fields)

    train_iter = MyIterator(train, batch_size=opt.batchsize, device=opt.device,
                            repeat=False, sort_key=lambda x: (len(x.src), len(x.trg)),
                            batch_size_fn=batch_size_fn, train=True, shuffle=True)

    SRC.build_vocab(train)
    TRG.build_vocab(train)

    opt.src_pad = SRC.vocab.stoi['<pad>']
    opt.trg_pad = TRG.vocab.stoi['<pad>']

    opt.train_len = get_len(train_iter)

    return train_iter


def get_len(train):
    """
    Вернуть количество циклов
    :param train:
    :return: количество циклов
    """
    for i, b in enumerate(train):
        pass

    return i
