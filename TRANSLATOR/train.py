import argparse
import time
import torch
from Models import get_model
from Process import *
import torch.nn.functional as F
from Batch import create_masks
import dill as pickle
from globals import DATA_PATH
from spacy.cli import download


def train_model(model, opt):
    """
    Обучение
    :param model: модель
    :param opt: параметры
    :return:
    """
    print("Обучение модели...")
    model.train()
    start = time.time()

    for epoch in range(opt.epochs):

        total_loss = 0
        print("   %dm: epoch %d [%s]  %d%%  loss = %s" % \
              ((time.time() - start) // 60, epoch + 1, "".join(' ' * 20), 0, '...'), end='\r')

        for i, batch in enumerate(opt.train):

            src = batch.src.transpose(0, 1).to(opt.device)
            trg = batch.trg.transpose(0, 1).to(opt.device)
            trg_input = trg[:, :-1]
            src_mask, trg_mask = create_masks(src, trg_input, opt)
            src_mask.to(opt.device)
            trg_mask.to(opt.device)
            preds = model(src, trg_input, src_mask, trg_mask)
            ys = trg[:, 1:].contiguous().view(-1)
            opt.optimizer.zero_grad()
            loss = F.cross_entropy(preds.view(-1, preds.size(-1)), ys, ignore_index=opt.trg_pad)
            loss.backward()
            opt.optimizer.step()

            total_loss += loss.item()

            if (i + 1) % opt.printevery == 0:
                p = int(100 * (i + 1) / opt.train_len)
                avg_loss = total_loss / opt.printevery
                print("   %dm: epoch %d [%s%s]  %d%%  loss = %.3f" % \
                      ((time.time() - start) // 60, epoch + 1, "".join('#' * (p // 5)),
                       "".join(' ' * (20 - (p // 5))), p, avg_loss), end='\r')
                total_loss = 0

        print("%dm: epoch %d [%s%s]  %d%%  loss = %.3f\nepoch %d complete, loss = %.03f" % \
              ((time.time() - start) // 60, epoch + 1, "".join('#' * (100 // 5)), "".join(' ' * (20 - (100 // 5))), 100,
               avg_loss, epoch + 1, avg_loss))


def main():
    """
    Основная функция
    """
    # Словари загружать 1 раз
    #download("en_core_web_md")
    #download("ru_core_news_md")

    # Параметры командной строки по умолчанию
    parser = argparse.ArgumentParser()
    # Файлы корпуса и код языка
    parser.add_argument('-src_data', default=DATA_PATH + 'ru.txt')
    parser.add_argument('-trg_data', default=DATA_PATH + 'en.txt')
    parser.add_argument('-src_lang', default='ru')
    parser.add_argument('-trg_lang', default='en')
    # Принудительно отключать GPU
    parser.add_argument('-no_cuda', action='store_true', default=False)
    # Количество циклов
    parser.add_argument('-epochs', type=int, default=5)
    # d_model: размер матрицы
    parser.add_argument('-d_model', type=int, default=512)
    # количество слоев
    parser.add_argument('-n_layers', type=int, default=6)
    # heads: количество "голов" для механизма внимания (default=8).
    parser.add_argument('-heads', type=int, default=8)
    # dropout
    parser.add_argument('-dropout', type=int, default=0.1)
    # размер партии
    parser.add_argument('-batchsize', type=int, default=1500)
    # Вывод состояния обучения на экран каждые...
    parser.add_argument('-printevery', type=int, default=10)
    # погрешность вычисления
    parser.add_argument('-lr', type=int, default=0.0001)
    # Путь к сохраненным "весам" ( при  обучении)
    parser.add_argument('-load_weights')
    # максимальная длина предложения
    parser.add_argument('-max_strlen', type=int, default=150)

    opt = parser.parse_args()

    # определяем наличие графич. адаптера NVIDIA
    gpu = torch.cuda.is_available()
    if gpu is False:
        opt.device = 'cpu'
    else:
        opt.device = 'cuda'

    if opt.no_cuda is True:
        opt.device = 'cpu'

    # Загружаем корпус
    read_data(opt)
    SRC, TRG = create_fields(opt)
    # создаем словари для обучения
    opt.train = create_dataset(opt, SRC, TRG)
    print(f"количество уникальных слов для обучения (RU): {len(SRC.vocab)}")
    print(f"количество уникальных слов для обучения (EN): {len(TRG.vocab)}")
    model = get_model(opt, len(SRC.vocab), len(TRG.vocab))

    opt.optimizer = torch.optim.Adam(model.parameters(), lr=opt.lr, betas=(0.9, 0.98), eps=1e-9)

    train_model(model, opt)

    promptNextAction(model, opt, SRC, TRG)


def yesno(response):
    """
    обработка команд с клавиатуры
    :param response: ответ
    :return:
    """
    while True:
        if response != 'y' and response != 'n':
            response = input('команда не распознана, введите y or n : ')
        else:
            return response


def promptNextAction(model, opt, SRC, TRG):
    """
    Диалог обработки
    :param model:
    :param opt:
    :param SRC:
    :param TRG:
    :return:
    """
    saved_once = 0

    while True:
        save = yesno(input('Обучение завершено, сохранить результат? [y/n] : '))
        if save == 'y':
            print("сохраняем weights в " + DATA_PATH + "...")
            torch.save(model.state_dict(), f'{DATA_PATH}/model_weights')
            if saved_once == 0:
                pickle.dump(SRC, open(f'{DATA_PATH}/SRC.pkl', 'wb'))
                pickle.dump(TRG, open(f'{DATA_PATH}/TRG.pkl', 'wb'))
                saved_once = 1

            print("weights и  pickles сохранены в " + DATA_PATH)

        res = yesno(input("продолжнить обучение? [y/n] : "))
        if res == 'y':
            while True:
                epochs = input("введите количество циклов : ")
                try:
                    epochs = int(epochs)
                except:
                    print("введите цифру")
                    continue
                if epochs < 1:
                    print("введите число > 0")
                    continue
                else:
                    break
            opt.epochs = epochs
            train_model(model, opt)
        else:
            print("выходим...")
            break


if __name__ == "__main__":
    main()
