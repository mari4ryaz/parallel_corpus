import torch
from Process import *
import random
import argparse
from Models import get_model
from Beam import beam_search
from nltk.corpus import wordnet
from torch.autograd import Variable
from globals import DATA_PATH
import re
import pandas as df


def init_translat():
    """
    Инициализация переводчика
    :return: наборы данных, класс модели, параметры
    """
    # Параметры командной строки по умолчанию
    parser = argparse.ArgumentParser()
    # Путь к сохраненным "весам" ( при  обучении)
    parser.add_argument('-load_weights', default=DATA_PATH)
    parser.add_argument('-k', type=int, default=3)
    # максимальная длина предложения
    parser.add_argument('-max_len', type=int, default=80)
    # d_model: размер матрицы
    parser.add_argument('-d_model', type=int, default=512)
    # количество слоев
    parser.add_argument('-n_layers', type=int, default=6)
    # Файлы корпуса и код языка
    parser.add_argument('-src_lang', default='ru')
    parser.add_argument('-trg_lang', default='en')
    # heads: количество "голов" для механизма внимания (default=8).
    parser.add_argument('-heads', type=int, default=8)
    # dropout
    parser.add_argument('-dropout', type=int, default=0.1)
    # Принудительно отключать GPU
    parser.add_argument('-no_cuda', action='store_true', default=False)
    # Получение аргументов с командной строки
    opt = parser.parse_args()
    # Определение графического адаптера  NVIDIA
    opt.no_cuda = False
    opt.device = 'cuda' if opt.no_cuda is False else 'cpu'
    if opt.device == 'cuda':
        assert torch.cuda.is_available()
    # Проверка на то, что переданные пвраметры соответсвуют условиям
    assert opt.k > 0
    assert opt.max_len > 10
    # Создание наборов данных
    SRC, TRG = create_fields(opt)
    model = get_model(opt, len(SRC.vocab), len(TRG.vocab))
    return SRC, TRG, model, opt


def get_translated(opt, model, SRC, TRG, s):
    """
        Получение пререведенного текста
        :param opt:  параметры
        :param model: ссылка на модель
        :param SRC: исходный словарь
        :param TRG: целевой словарь
        :param s: текст для перевода
        :return: переведенная строка
    """
    opt.text = s
    phrase = translate(opt, model, SRC, TRG)
    return phrase


def get_synonym(word, SRC):
    """
        Поиск синонима слова
        :param word: слово
        :param SRC: словарь
        :return: синоним
    """
    syns = wordnet.synsets(word)
    for s in syns:
        for l in s.lemmas():
            if SRC.vocab.stoi[l.name()] != 0:
                return SRC.vocab.stoi[l.name()]

    return 0


def multiple_replace(dict, text):
    """
        Замена слов по словарю
        :param dict: словарь
        :param text: слово
        :return:
    """
    # создание регулярного выражения
    regex = re.compile("(%s)" % "|".join(map(re.escape, dict.keys())))

    # поиск в словаре
    return regex.sub(lambda mo: dict[mo.string[mo.start():mo.end()]], text)


def translate_sentence(sentence, model, opt, SRC, TRG):
    """
        Перевод предложения
        :param sentence:  предложение
        :param model: ссылка на модель
        :param opt: параметры
        :param SRC: исходный словарь
        :param TRG: целевой словарь
        :return: перевод предложения
    """
    model.eval()
    indexed = []
    sentence = SRC.preprocess(sentence)
    for tok in sentence:
        if SRC.vocab.stoi[tok] != 0:
            indexed.append(SRC.vocab.stoi[tok])
        else:
            indexed.append(get_synonym(tok, SRC))
    sentence = Variable(torch.LongTensor([indexed]))
    if opt.device == 'cuda':
        sentence = sentence.cuda()

    sentence = beam_search(sentence, model, SRC, TRG, opt)

    try:
        sent = multiple_replace({' ?': '?', ' !': '!', ' .': '.', '\' ': '\'', ' ,': ','}, sentence)
    except:
        sent = sentence
    return sent


def translate(opt, model, SRC, TRG):
    """
        Перевод тектса
        :param opt: параметры
        :param model: ссылка на модель
        :param SRC: исходный словарь
        :param TRG: целевой словарь
        :return: переведенный текст
    """
    sentences = opt.text.lower().split('.')
    translated = []

    for sentence in sentences:
        if sentence != '':
            translated.append(translate_sentence(sentence + '.', model, opt, SRC, TRG))  # .capitalize())

    try:
        s = (' '.join(translated))
    except:
        s = ''
    return s


def calculate_bleu(opt, model, SRC, TRG):
    """

    :param opt:
    :param model:
    :param SRC:
    :param TRG:
    :return:
    """
    trgs = []
    pred_trgs = []

    lines = df.read_csv('translate_transformer_temp.csv', encoding='utf-8')
    # Split every line into pairs and normalize

    pairs = [list(row) for row in lines.values]

    path = DATA_PATH + 'src.txt'
    src = open(path, 'a+', encoding='utf-8')
    path = DATA_PATH + 'res.txt'
    res = open(path, 'a+', encoding='utf-8')
    path = DATA_PATH + 'full.txt'
    full = open(path, 'a+', encoding='utf-8')

    for i in range(50):
        pair = random.choice(pairs)

        opt.text = pair[0]
        pred_trg = translate(opt, model, SRC, TRG)
        print('>', pair[0])
        print('=', pair[1])
        print('<', pred_trg)
        print('\n')
        if pred_trg != '':
            full.write(pair[1] + '\t' + pred_trg + '\n')
            src.write(pair[1] + '\n')
            res.write(pred_trg + '\n')
        pred_trgs.append(pred_trg.strip().split(' '))
        trgs.append(pair[1].strip().split(' '))
    res.close()
    src.close()
    full.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-load_weights', default=DATA_PATH)
    parser.add_argument('-k', type=int, default=3)
    parser.add_argument('-max_len', type=int, default=80)
    parser.add_argument('-d_model', type=int, default=512)
    parser.add_argument('-n_layers', type=int, default=6)
    parser.add_argument('-src_lang', default='ru')
    parser.add_argument('-trg_lang', default='en')
    parser.add_argument('-heads', type=int, default=8)
    parser.add_argument('-dropout', type=int, default=0.1)
    parser.add_argument('-no_cuda', action='store_true', default=False)
    parser.add_argument('-floyd', action='store_true')

    opt = parser.parse_args()

    opt.device = 'cuda'

    assert opt.k > 0
    assert opt.max_len > 10

    SRC, TRG = create_fields(opt)
    model = get_model(opt, len(SRC.vocab), len(TRG.vocab))

    while True:
        opt.text = input("Enter a sentence to translate (type 'f' to load from file, or 'q' to quit):\n")
        if opt.text == "q":
            break
        if opt.text == 'f':
            fpath = input("Enter a sentence to translate (type 'b' to save data for bleu_score, or 'q' to quit):\n")
            try:
                opt.text = ' '.join(open(opt.text, encoding='utf-8').read().split('\n'))
            except:
                print("error opening or reading text file")
                continue
        if opt.text == "b":
            calculate_bleu(opt, model, SRC, TRG)

        phrase = translate(opt, model, SRC, TRG)
        print('> ' + phrase + '\n')


if __name__ == '__main__':
    main()
