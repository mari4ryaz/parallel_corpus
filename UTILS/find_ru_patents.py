import sys
import time
import requests
import os
import platform
from globals import DATA_PATH
from io import BytesIO
import pandas as pd
from DRIVERS.driver import PatentWebDriver
from DRIVERS.patent import GooglePatent
from DB.db_mysql_driver import MySQLDriver
from DB.db_clickhouse_driver import ClickHouseDriver
from globals import MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE
from globals import HOUSE_HOST, HOUSE_PORT, HOUSE_USER, HOUSE_PASSWORD, HOUSE_DATABASE

# Ищет патенты на русском языке по классу CPC
# потом для каждого найденного патента ищем ссылку на англ вариант
# найденные ссылки сохраняются в таблицу БД -links


initial_load = 0  # Флаг: начальная загрузка


def get_patent_links(cpc, path):
    """
    Получение ссылки на патенты (из csv  файла)
    :param cpc: категория
    :param path: путь к файлу
    :return:  список ссылок
    """
    patent_links = pd.read_csv(path + cpc + "_links.csv", skiprows=1)['result link'].tolist()
    return patent_links


def get_doc_patents(cpc, path):
    """
    Получение csv файла с сайта Google со списком рускоязычных патентов заданной категории
    :param cpc:  категория
    :param path: путь для сохранения файла
    :return: csv файл с сайта Google Patents
    """
    headers = {
        'authority': 'patents.google.com',
        'sec-ch-ua': '"Google Chrome";v="95", "Chromium";v="95", ";Not A Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'x-client-data': 'CJW2yQEIpbbJAQjBtskBCKmdygEIk9vKAQjq8ssBCO/yywEInvnLAQjmhMwBCLWFzAEI/4XMAQjLicwBCI6NzAEYqqnKAQ==',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-dest': 'document',
        'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
        'cookie': 'CONSENT=YES+RU.ru+20180311-09-0; ANID=AHWqTUnhydZcNb0Bi9Z2Dol2Fzbfl1sluC1hnyrtfzPNgmk9q2DbO2NGogTpoolK; OGP=-19022591:; __Secure-1PSIDCC=AJi4QfF9uhf29mMsiIaTG7SvO9xaxgz5_i7tJy7kCzsrH1lkOYq8TnvxOwPAweVM-NcPHuXv; HSID=AUf9kWdpkVfcIEwHj; SSID=AHQOR7k5hrQrcWDy2; APISID=rHXMcwYERlmkcJC8/AJ_lEhLnHtSeK12Ee; SAPISID=lneiZkJX8MbzsnpN/AmfmLHsatBHuKK1i_; __Secure-1PAPISID=lneiZkJX8MbzsnpN/AmfmLHsatBHuKK1i_; __Secure-3PAPISID=lneiZkJX8MbzsnpN/AmfmLHsatBHuKK1i_; _ga=GA1.3.1424021396.1631281732; OGPC=19022622-1:; SID=DQgzj7EJRUqoydE9cRjzg5mrgmbgHb0Zo4b6T08xNRiMRKig1NpV0kG5k22OlYKBOHT0cg.; __Secure-1PSID=DQgzj7EJRUqoydE9cRjzg5mrgmbgHb0Zo4b6T08xNRiMRKigLOqnIwPV83QDwfidVBYS8Q.; __Secure-3PSID=DQgzj7EJRUqoydE9cRjzg5mrgmbgHb0Zo4b6T08xNRiMRKigCpoBWLxXnj_r4kRMMohdhw.; S=sso=Pq1sbhI3Cxt7QJFCnYy9FxiGnyuyFlrA; SEARCH_SAMESITE=CgQIg5QB; NID=511=P6MxVTrB07JPZAPoX1ISDORTKkng_1yGNdL6TfmdzbyEuUDchoXgxDB3kM4TMuykPV8VdP4dctB2SKjLrUmmu6Xk1V9CCR-lXhVQwKgpO4f5ZHJ7DFGNv52RziIlr4aT8jXwEEB3N8rUlqXDPk5Bv3jv2AyMSsS9cTvZ9zhTOZdAqfnxIbAmLbUrjLLHKZJeudoT0IkcyIfa2UbNAQbTwdw5MOrFMiXQ-rzeyW9ouXB1UWGi3WCXLpEe70wXn38PKBIImo1PQBS7GaC1qbNL_bDCl68Yt6TcJIEiZoluGiJ2P1BnoOnNenSTbFEVTUsTgpcXG3Y4PatPS6ESXR1Jl--TUmkOCidOA9QHyrwxo6ibMlNUxCW25nke_OYwdedJHdOdpnGlRLg73TQMiybm5MPHL7lCwySAmPPTiDFNVhjvNvEPW0oxhdkzc4Hc4YLw-PZZpAEod9zRGX0qTg; 1P_JAR=2021-11-16-19; SIDCC=AJi4QfGGwRvULEgLIhz7vaDrPkLZva7ep6afDZkFkwBD3MY3vlGf96n9hH-HPJjtC1t3-K_3muhl; __Secure-3PSIDCC=AJi4QfERPdkXEUEQzGAAyXJbehDh1EIb_A-d7zzxvNba8fVbmSlA93b9lcTvSLXDvBUAi4pAmeg',
    }

    params = (
        ('url',
         'q=CPC%3d' + cpc + '%2flow&language=RUSSIAN'),
        ('exp', ''),
        ('download', 'true'),
        ('download_format', 'csv')
    )

    response = requests.get('https://patents.google.com/xhr/query', headers=headers, params=params).content

    cpc1 = cpc.replace('/', '-')

    open(path + cpc1 + "_links.csv", "wb").write(response)


def create_links_patents(cpc_classes):
    """
    Получение ссылок на патенты
    :param cpc_classes: МПК классы патентов
    :return: список ссылок на патенты
    """
    all_patent_links = []

    for cpc in cpc_classes:
        cpc1 = cpc.replace('/', '-')
        save_dir = f'{DATA_PATH}{cpc1}'
        if not os.path.isdir(save_dir):
            os.mkdir(save_dir)
            print("Создание директории [" + cpc1 + "]")
        if os.path.isfile(save_dir + cpc1 + "_links.csv"):
            patent_links = get_patent_links(cpc=cpc1, path=save_dir)
        else:
            get_doc_patents(cpc=cpc, path=save_dir)
            patent_links = get_patent_links(cpc=cpc1, path=save_dir)
        all_patent_links.extend(patent_links)
        print("cpc: " + "[" + cpc + "]")
        print("Кол-во патентов: " + "[" + str(len(patent_links)) + "]")
    print("Общее кол-во ссылок: " + str(len(all_patent_links)))
    return all_patent_links


def main():
    # Подключение к базе данных
    osp = platform.platform()
    if osp.startswith('Windows') == 1:
        db_cursor = MySQLDriver(MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE)
    else:
        db_cursor = ClickHouseDriver(HOUSE_HOST, HOUSE_PORT, HOUSE_USER, HOUSE_PASSWORD, HOUSE_DATABASE)

    db_cursor.connect_db()
    if initial_load == 1: # очищаем все ранее загруженные данные
        db_cursor.delete_all_from_db()

    # Получение списка патентов из бд
    start_time = time.monotonic()
    # Классы патентов для загрузки
    cpc_classes = ["G06N", "G06F"]
    # Получение списка ссылок на патенты в массив
    all_patent_links = create_links_patents(cpc_classes=cpc_classes)
    driver = PatentWebDriver()
    patent = GooglePatent(driver)
    # Обработка
    for url in all_patent_links:
        # номер патента из ссылки
        patent_num = url.replace('https://patents.google.com/patent/', '')
        patent_num = patent_num.replace('/ru', '')
        #  Уже загружали?
        result = db_cursor.is_patent_exists(patent_num)
        if not result:
            # Загрузка патента с сайта
            try:
                patent.load_data(url)
                # Получение номера англ. патента и ссылку на него
                uspatent, uspatentlink = patent.get_us_patent()
                # Сохранение страницы в БД
                db_cursor.insert_html(patent_num, patent.get_data())
                # Сохранение ссылок в БД
                db_cursor.insert_base(patent_num, uspatent, uspatentlink, url)
                if uspatent != '':
                    # Повторяем для англ. патента
                    patent.load_data(uspatentlink)
                    db_cursor.insert_html(uspatent, patent.get_data())
                print("[ Патент " + patent_num + " добавлен в БД ]")
            except Exception as e:
                print(e)
                driver.quit()
                sys.exit(1)
        else:
            print("[ Патент " + patent_num + " уже есть в БД ]")

    t = time.monotonic() - start_time
    print('парсинг окончен [' + str(t) + ']')


if __name__ == "__main__":
    main()
