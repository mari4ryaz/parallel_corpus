import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
PARSER_PATH = ROOT_DIR
DATA_PATH = os.path.join(ROOT_DIR, 'DATA', '')
TEMP_PATH = os.path.join(ROOT_DIR, 'TEMP', '')

DEBUG_FLAG = 0

MYSQL_HOST = "localhost"
MYSQL_PORT = 3306
MYSQL_USER = "test"
MYSQL_PASSWORD = "12345"
MYSQL_DATABASE = "patents"

HOUSE_HOST = "localhost"
HOUSE_PORT = 3306
HOUSE_USER = "default"
HOUSE_PASSWORD = "12345"
HOUSE_DATABASE = "patents"